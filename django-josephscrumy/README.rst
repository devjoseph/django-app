This's my django application for linuxjobber internship program

Quick Start
------------

add "josephscrumy" in your INSTALLED_APP like this:
        INSTALLED_APP = [
            ---
            'josephscrumy',
        ]

The application url is should be included in your project urls.py like this:
        path('josephscrumy/', include('josephscrumy.urls'))

the run "python manage.py migrate". to migrate your database

run your server 

and visit this link http://127.0.0.1:8000/josephscrumy/
(to see the application homepage)