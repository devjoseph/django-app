import os
from setuptools import find_packages, setup

with open(os.path.join(os.path.dirname(__file__), 'README.rst')) as readme:
    README = readme.read()

# Allow setup.py file to be run from any path
os.chdir(os.path.normpath(os.path.join(os.path.abspath(__file__), os.pardir)))

setup(
    name = 'josephscrumy',
    version = '0.1',
    packages = find_packages(),
    include_package_data = True,
    license = 'BSD License', 
    discription = 'A simple django app',
    long_description = README,
    url = 'http://18.224.137.20:8000/josephscrumy/',
    author = "Joseph Chinedu",
    author_email = "joseph4jubilant@gmail.com",
    classifiers = [
        'Environment :: Web Environment',
        'Framework :: Django',
        'Framework :: Django :: X.Y', 
        'Intended Audience :: Developers',
        'License: OSI Approved :: BSD License',
        'Operating System :: OS Independent',
        'Programming Language :: python',
        'Programming Language :: python :: 3.6',
        'Topic :: Internet :: WWW/HTTP',
        'Topic :: Internet :: WWW/HTTP :: Dynamic Content',
    ],

)